package com.upb.alertapp.model.entities;

import com.google.android.gms.maps.model.LatLng;

public class User {

	LatLng position;
	int zoom;

	public User(LatLng position, int zoom) {
		super();
		this.position = position;
		this.zoom = zoom;
	}

	public LatLng getPosition() {
		return position;
	}

	public void setPosition(LatLng position) {
		this.position = position;
	}

	public int getZoom() {
		return zoom;
	}

	public void setZoom(int zoom) {
		this.zoom = zoom;
	}

}
