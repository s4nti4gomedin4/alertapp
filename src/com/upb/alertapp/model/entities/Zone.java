package com.upb.alertapp.model.entities;
import com.google.android.gms.maps.model.LatLng;
import com.upb.alertapp.helper.Utilities;

public class Zone {
	 private LatLng position;
	 private String nombre;
	 private int dangerLevel;
	 private int radio;
	 
	 public String toString(){
		 return nombre+" ("+Utilities.parseDanger(dangerLevel)+")";
	 }
	 
	 public Zone(LatLng position,String nombre,int danger,int radio){
		 this.position=position;
		 this.nombre=nombre;
		 this.dangerLevel=danger;
		 this.radio=radio;
	 }
	public int getRadio() {
		return radio;
	}
	public void setRadio(int radio) {
		this.radio = radio;
	}
	public LatLng getPosition() {
		return position;
	}
	public void setPosition(LatLng position) {
		this.position = position;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getDangerLevel() {
		return dangerLevel;
	}
	public void setDangerLevel(int dangerLevel) {
		this.dangerLevel = dangerLevel;
	}
	 
}
