package com.upb.alertapp;

import java.util.ArrayList;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.upb.alertapp.helper.Utilities;
import com.upb.alertapp.model.entities.Zone;

public class AlertAppActivity extends Activity {

	static final LatLng MEDELLIN = new LatLng(6.250727, -75.572346);
	private GoogleMap map;
	private Marker markerLocation = null;
	private Circle circleCurrent = null;
	private ListView lv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_alert_app);
		MapFragment viewMapFragment = (MapFragment) getFragmentManager()
				.findFragmentById(R.id.map);
		map = viewMapFragment.getMap();

		if (map != null) {
			ArrayList<Zone> zonas = Utilities.getZones();
			for (Zone zona : zonas) {

				CircleOptions circleDanger = new CircleOptions()
						.center(zona.getPosition()).radius(zona.getRadio())
						.fillColor(zona.getDangerLevel()).strokeWidth(0);
				Circle cir=map.addCircle(circleDanger);
			}
			markerLocation = map.addMarker(new MarkerOptions()
					.position(MEDELLIN)
					.title("Medellin")
					.snippet("Medellin pues.")
					.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.ic_person)));

			addCircleToMap(MEDELLIN);

			map.moveCamera(CameraUpdateFactory.newLatLngZoom(MEDELLIN, 12));
			map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

			onMapMarkerTouch();

		}
		
		 lv = (ListView) findViewById(R.id.ZoneList);
		 ArrayAdapter<Zone> arrayAdapter = new ArrayAdapter<Zone>(
				                  this, 
				                 android.R.layout.simple_list_item_1,
				                  Utilities.getZones()
				                   );
				 
				          lv.setAdapter(arrayAdapter); 
				          lv.setOnItemClickListener(new OnItemClickListener() {
				 
				 			@Override
				 			public void onItemClick(AdapterView<?> arg0, View item, int index,
				 					long arg3) {
				 				// TODO Auto-generated method stub
				 				
				 				map.animateCamera(CameraUpdateFactory.newLatLng(Utilities.getZones().get(index).getPosition()),
				 						1000, null);
				 
				 				
				 			}
				         	 
				 		});
				 		
	}

	public void onMapMarkerTouch() {

		map.setOnMapClickListener(new OnMapClickListener() {

			@Override
			public void onMapClick(LatLng location) {

				markerLocation.remove();
				markerLocation = map.addMarker(new MarkerOptions()
						.position(location)
						.title("Aqui estas")
						.icon(BitmapDescriptorFactory
								.fromResource(R.drawable.ic_person)));

				circleCurrent.remove();
				addCircleToMap(location);
				map.animateCamera(CameraUpdateFactory.newLatLng(location),
						1000, null);

				
				for (Zone zona : Utilities.getZones()) {
					if(checkZoneCollide(zona,location,Utilities.userRadius)){
						if(zona.getDangerLevel()==Utilities.dangerZone){
							Toast.makeText(getApplicationContext(), "Entrando a zona peligrosa de "+zona.getNombre(), Toast.LENGTH_SHORT).show();
						}else if(zona.getDangerLevel()==Utilities.warningZone){
							Toast.makeText(getApplicationContext(), "Entrando a zona con incidentes delictivos de  "+zona.getNombre(), Toast.LENGTH_SHORT).show();
						}else if(zona.getDangerLevel()==Utilities.safeZone){
							Toast.makeText(getApplicationContext(), "Entrando a zona segura de "+zona.getNombre(), Toast.LENGTH_SHORT).show();
						}else{
							Toast.makeText(getApplicationContext(), "Zona desconocida."+zona.getNombre(), Toast.LENGTH_SHORT).show();
						}
						break;
					}
				}
				
				
			}

		
		});

	}
	protected boolean checkZoneCollide(Zone zona, LatLng location,
			int userradius) {
		float[] results = new float[1];
		int ratioSum=zona.getRadio()+userradius;
		Location.distanceBetween(zona.getPosition().latitude, zona.getPosition().longitude, location.latitude, location.longitude, results);
		return results[0]<ratioSum;
		
	}

	public void addCircleToMap(LatLng location) {

		CircleOptions circleCurrentMarker = new CircleOptions()
				.center(location).radius(Utilities.userRadius)
				.fillColor(Utilities.userZone).strokeWidth(0);

		circleCurrent = map.addCircle(circleCurrentMarker);
		


	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.alert_app, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_select) {
			showList();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void showList() {
		if(lv.getVisibility()==View.GONE){
			lv.setVisibility(View.VISIBLE);
		}else{
			lv.setVisibility(View.GONE);
		}
		
	}
}
