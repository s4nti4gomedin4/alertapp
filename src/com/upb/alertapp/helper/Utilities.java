package com.upb.alertapp.helper;

import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.gms.maps.model.LatLng;
import com.upb.alertapp.model.entities.User;
import com.upb.alertapp.model.entities.Zone;

public class Utilities {

	public final static int dangerZone = 0x40FF0000;
	public final static int safeZone = 0x4000FF00;
	public final static int warningZone = 0x40FAAC05;
	public final static int userZone = 0x400000FF;
	public final static int userRadius = 350;
	public static String parseDanger(int danger) {
	  switch (danger) {
	case dangerZone:
		return "Peligro";
		
	case safeZone:
		return "Segura";
	case warningZone:
		return "Incidentes";
		
	default:
		return "Desconocida";
				}
	}
	public static ArrayList<Zone> getZones() {
		ArrayList<Zone> lista = new ArrayList<Zone>();

		lista.add(new Zone(new LatLng(6.291680, -75.596121), "Aures",
				dangerZone, 800));
		lista.add(new Zone(new LatLng(6.21667, -75.5667), "Poblado", safeZone,
				1000));
		lista.add(new Zone(new LatLng(6.301730, -75.585542), "Picacho",
				dangerZone, 900));
		lista.add(new Zone(new LatLng(6.307766, -75.577088), "12 de Octubre",
				warningZone, 1000));
		lista.add(new Zone(new LatLng(6.298637, -75.544515),
				"Santo Domingo Sabio", warningZone, 1200));
		lista.add(new Zone(new LatLng(6.264725, -75.550695), "Manrrique",
				dangerZone, 800));
		lista.add(new Zone(new LatLng(6.239982, -75.591164), "Laureles",
				safeZone, 1200));
		lista.add(new Zone(new LatLng(6.257942, -75.589533), "Estadio",
				safeZone, 900));
		lista.add(new Zone(new LatLng(6.250988, -75.571981), "La Candelaria",
				warningZone, 900));
		lista.add(new Zone(new LatLng(6.2223944, -75.5873266),
				"Barrio Antioquia", dangerZone, 600));
		return lista;
	}

	public User getUserData(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				"location", 0);

		double lat = Double.valueOf(sharedPreferences.getString("lat", "0"));

		double lng = Double.valueOf(sharedPreferences.getString("lng", "0"));

		int zoom = Integer.valueOf(sharedPreferences.getString("zoom", "0"));
		User user = new User(new LatLng(lat, lng), zoom);
		return user;
	}

	public void saveUserData(Context context, User user) {
		SharedPreferences.Editor editor = context.getSharedPreferences(
				"location", 0).edit();

		editor.putString("lat", Double.toString(user.getPosition().latitude));

		editor.putString("lng", Double.toString(user.getPosition().longitude));

		editor.putString("zoom", Float.toString(user.getZoom()));

		editor.commit();

	}
}
